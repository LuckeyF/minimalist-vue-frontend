import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";


export const useUserStore = defineStore("user", () => {

	const users = ref([]);

	async function newUser(newUser) {
		let response = await axios.post("https://jvxvihkomp.user-management.asw.rest/api/users", newUser);
		console.log("User creation successful: ", response.data);
	}

	async function getAllUsers() {
		let response = await axios.get("https://jvxvihkomp.user-management.asw.rest/api/users");
		users.value = response.data;
		console.log("All Users:", response.data);
	}

	return {users, newUser, getAllUsers}


});
